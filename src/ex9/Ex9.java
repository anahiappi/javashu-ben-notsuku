package ex9;

public class Ex9 {
	public static void main(String[] args) {
		IceCream ice = new IceCream("ポッピン〇シャワー");
		ice.printflavor();

		DoubleIceCream dIce = new DoubleIceCream("キャラメルリボン", "大納言あずき");
		dIce.printflavor();
	}
}

class IceCream {
	protected String flavor;
	IceCream(String flavor) {
		this.flavor = flavor;
	}
	void printflavor() {
		System.out.println("シングルの"+flavor+"です");
	}
}

class DoubleIceCream extends IceCream {
	protected String flavor2;
	DoubleIceCream(String flavor, String flavor2) {
		super(flavor);
		this.flavor2 = flavor2;
	}
	void printflavor() {
		System.out.println("ダブルの"+flavor+"&"+flavor2+"です");
	}
}
