//継承の練習。カエルの鳴き声をオーバライド
package ex11;

public class Ex11 {
	public static void main(String[] args) {
		new UshiGaeru().croaks();
	}
	static class Frog {
		void croaks() {
			System.out.println("ケロケロ");
		}
	}
	static class UshiGaeru extends Frog {
		void croaks() {
			System.out.println("げろろ");
		}
	}
}
