package ex10;

public class Ex10 {
	public static void main(String[] args) {
		Microwave sharp = new Microwave("SHARP");
		sharp.heatUp(30);

		MicrowaveOven toshiba = new MicrowaveOven("TOSHIBA");
		toshiba.bake(120);
		toshiba.heatUp(20);
	}
}

class Microwave {
	Microwave(String manufacturer) {
		System.out.println(manufacturer+"製の電子レンジです。");
	}
	void heatUp(int second) {
		System.out.println(second+"秒、チンします。");
	}
}

class MicrowaveOven extends Microwave {
	MicrowaveOven(String manufacturer) {
		super(manufacturer);
		System.out.println("オーブンにもなるよ。");
	}
	void bake(int second) {
		System.out.println(second+"秒、オーブンで焼きます。");
	}
}