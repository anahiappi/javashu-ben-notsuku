//動的な方法で、「4月1日(曜日)」～「4月30日(曜日)」を表示
package ex12;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

public class Ex12 {
	public static void main(String[] args) {
		Stream.iterate(LocalDate.of(2018,4,1), ld->ld.plusDays(1))
		.limit(30)
		.map(ld->ld.format(DateTimeFormatter.ofPattern("M月d日(E)")))
		.forEach(System.out::println);
	}
}
