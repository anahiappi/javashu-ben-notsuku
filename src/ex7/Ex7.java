package ex7;

public class Ex7 {
	public static void main(String[] args) {
		Member mem = new Member("はな子",18);
		mem.printInfo();
	}
}

class Member {
	String name;	//氏名
	int age;		//年齢
	void printInfo() {
		System.out.println("氏名 = "+name);
		System.out.println("年齢 = "+age);
	}
	Member(String name, int age) {
		this.name = name;
		this.age = age;
	}
}
