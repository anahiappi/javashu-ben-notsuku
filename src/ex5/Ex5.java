package ex5;

import java.util.Random;

//人をランダムに並べる
public class Ex5 {
	public static void main(String[] args) {
		String[] nameArray = {"佐藤","伊藤","加藤","後藤","工藤"};
		//Fisher-Yatesアルゴリズムでランダムに並べる
		for(int i = nameArray.length -1 ; i > 0; i--) {
			int r = new Random().nextInt(i);
			String tmp = nameArray[r];
			nameArray[r] = nameArray[i];
			nameArray[i] = tmp;
		}
		//配列を表示
		for(int i = 0; i < nameArray.length; i++) {
			System.out.println(nameArray[i]);
		}
	}
}
