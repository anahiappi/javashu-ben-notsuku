package ex6;

public class Ex6 {
	public static void main(String[] args) {
		Member mem = new Member();
		mem.name = "はな子";
		mem.age = 18;
		System.out.println("氏名 = "+mem.name);
		System.out.println("年齢 = "+mem.age);
	}
}

class Member {
	String name;	//氏名
	int age;		//年齢
}
