package ex1;

import java.time.LocalTime;

//午前は「午前です」と表示、午後は「午後です」と表示する
public class Ex1 {
	public static void main(String[] args) {
		int h = LocalTime.now().getHour();
		if(h < 12) {
			System.out.println("午前です");
		}else {
			System.out.println("午後です");
		}
	}
}
