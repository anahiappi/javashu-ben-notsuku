package ex2;

//n(3以上の任意の整数)までの3の倍数を表示するプログラム
public class Ex2 {
	public static void main(String[] args) {
		int n = 40;
		for (int i = 3; i < n; i += 3) {
			System.out.println(i);
		}
	}
}
