package ex4;

import java.util.Random;
import java.util.Scanner;

//じゃんけんゲーム
public class Ex4 {
	public static void main(String[] args) {
		System.out.print("出す手を入力してください(0...グー、1...チョキ、2...パー)>");

		//コンピュータの手(0...グー、1...チョキ、2...パー)
		int cpu = new Random().nextInt(3);
		//あなたの手(0...グー、1...チョキ、2...パー)
		int you = new Scanner(System.in).nextInt();

		//手の表示
		String[] hand = {"グー","チョキ","パー"};
		System.out.println("コンピュータの手は"+hand[cpu]+"です");
		System.out.println("あなたの手は"+hand[you]+"です");
		System.out.println();

		//勝敗判定
		switch ((you - cpu) % 3) {
		case 0:
			System.out.println("あいこ");
			break;
		case 1:
			System.out.println("コンピュータの勝ち");
			break;
		case 2:
			System.out.println("あなたの勝ち");
			break;
		default:
			break;
		}

	}
}
