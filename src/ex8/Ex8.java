package ex8;

public class Ex8 {
	public static void main(String[] args) {
		Member[] memArray = new Member[5];
		memArray[0] = new Member("はな子",18);
		memArray[1] = new Member("たろう",41);
		memArray[2] = new Member("じろう",30);
		memArray[3] = new Member("さぶろう",15);
		memArray[4] = new Member("シロー",7);

		for(int i = 0; i < memArray.length; i++) {
			if(memArray[i].age < 20) {
				memArray[i].printInfo();
			}
		}
		System.out.println("君たちはお酒が飲めません");
	}
}

class Member {
	String name;	//氏名
	int age;		//年齢
	void printInfo() {
		System.out.println("氏名 = "+name);
		System.out.println("年齢 = "+age);
	}
	Member(String name, int age) {
		this.name = name;
		this.age = age;
	}
}