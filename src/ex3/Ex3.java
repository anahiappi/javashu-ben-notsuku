package ex3;

//口座残高が1,000,000円以上の人の名前と残高を表示
public class Ex3 {
	public static void main(String[] args) {
		String[] nameArray = {"佐藤","伊藤","加藤","後藤","工藤"};
		int[] balanceArray = {2500000,300000,40000000,5500000,30000};
		//口座残高が1,000,000円以上の人の名前と残高を表示
		for(int i = 0; i < nameArray.length; i++) {
			if(balanceArray[i] >= 1000000) {
				System.out.println(nameArray[i]+", "+balanceArray[i]);
			}
		}
	}
}
